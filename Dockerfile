FROM   hwyuee/ubuntu:v2
ADD ./mountebank-v1.14.1-linux-x64.tar.gz /data/
WORKDIR /data/mountebank-v1.14.1-linux-x64
ADD stubs ./stubs/
EXPOSE 8080
EXPOSE 1109
ENTRYPOINT ./mb  --configfile ./stubs/imposters.json --allowInjection 